package it.uniroma3.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.controller.validator.AllievoValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.CentroService;

@Controller
public class AllievoController {
	
    @Autowired
    private AllievoService allievoService;
    
    @Autowired
    private CentroService centroService;

    @Autowired
    private AllievoValidator validator;
    
    /*@RequestMapping(value = "/allievo/{id}", method = RequestMethod.GET)
    public String getCustomer(@PathVariable("id") Long id, Model model) {
        model.addAttribute("allievo", this.allievoService.findById(id));
    	return "showCustomer";
    }*/
    
    @RequestMapping("/allievi")
    public String allAllievi(Model model) {
        model.addAttribute("allievi", this.allievoService.findAll());
        return "ListaAllievi";
    }

    @RequestMapping("/addAllievo")
    public String addAllievo(Model model) {
        model.addAttribute("allievo", new Allievo());
        model.addAttribute("centri", this.centroService.findAll());
        return "FormAllievo";
    }

    @RequestMapping(value = "/allievo", method = RequestMethod.POST)
    public String newAllievo(@Valid @ModelAttribute("allievo") Allievo allievo, 
    						BindingResult bindingResult, Model model) {
        this.validator.validate(allievo, bindingResult);
        
        if (this.centroService.numeroMax(allievo.getCentro())) {
        	model.addAttribute("numeroSuperato", "Il Centro e' al completo!");
        	return "FormAllievo";
        }
        
        if (this.allievoService.alreadyExists(allievo)) {
            model.addAttribute("esiste", "Questo allievo già esiste!");
            return "FormAllievo";
        }
        else {
            if (!bindingResult.hasErrors()) {
                this.allievoService.save(allievo);
                model.addAttribute("allievi", this.allievoService.findAll());
                return "ListaAllievi";
            }
        }
        model.addAttribute("centri", this.centroService.findAll());
        return "FormAllievo";
    }
    
    @RequestMapping(value="/allievi/{id}", method=RequestMethod.GET)
    public String descAllievo(@PathVariable("id") Long id, Model model) {
    	model.addAttribute("allievo", this.allievoService.findById(id));
		return "showAllievo";
    }
}