package it.uniroma3.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.uniroma3.controller.validator.AttivitaValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.CentroService;

@Controller
public class AttivitaController {
	
    @Autowired
    private AttivitaService attivitaService;
    
    @Autowired
    private AllievoService allievoService;
    
    @Autowired
    private CentroService centroService;

    @Autowired
    private AttivitaValidator validator;

    @RequestMapping("/listaAttivita")
    public String listaAttivita(Model model) {
        model.addAttribute("listaAttivita", this.attivitaService.findAll());
        return "ListaAttivita";
    }

    @RequestMapping("/addAttivita")
    public String addAttivita(Model model) {
        model.addAttribute("attivita", new Attivita());
        model.addAttribute("elencoCentri", this.centroService.findAll());
        return "FormAttivita";
    }

    @RequestMapping(value = "/attivita", method = RequestMethod.POST)
    public String newAttivita(@Valid @ModelAttribute("attivita") Attivita attivita, 
    		 					BindingResult bindingResult, Model model) {
        this.validator.validate(attivita, bindingResult);
        
        if (this.attivitaService.alreadyExists(attivita)) {
            model.addAttribute("esiste", "Questa attivtà già esiste!");
            return "FormAttivita";
        }
        else {
            if (!bindingResult.hasErrors()) {
                this.attivitaService.save(attivita);
                model.addAttribute("listaAttivita", this.attivitaService.findAll());
                return "ListaAttivita";
            }
        }
        model.addAttribute("elencoCentri", this.centroService.findAll());
        return "FormAttivita";
    }
    
    @RequestMapping(value="/addAllievoAttivita/{id}", method=RequestMethod.GET)
    public String addAllievoAttivita(@PathVariable("id") Long id, Model model) {
    	Attivita attivita = this.attivitaService.findById(id);
    	model.addAttribute("attDaRegistrarsi", attivita);
    	model.addAttribute("elencoAllievi", this.allievoService.findAll());
    	return "confermaAssociazione";
    }
    
    @RequestMapping(value = "/confermato/{id}/{idd}", method = RequestMethod.POST)
    public String allievoAttivita(@PathVariable("id") Long id,
    		@PathVariable("idd") Long idd, Model model) {
    	    	
    	Attivita attivita = this.attivitaService.findById(id);
    	Allievo allievo = this.allievoService.findById(idd);
    	
    	if(this.allievoService.alreadyExistsAtt(allievo, attivita)) {
    		model.addAttribute("giaAssociato", "Questo allievo e' gia' stato associato a questa attivita'");
    		return "confermaAssociazione";
    	}
    	else {
    		allievo.getAttivita().add(attivita);
    		attivita.getAllievi().add(allievo);
    	    	
    		this.allievoService.save(allievo);
    		this.attivitaService.save(attivita);
    		model.addAttribute("associato", "l'allievo e' stato associato all'attivita'");
    	
    		return "confermato";
    	}
    }
}