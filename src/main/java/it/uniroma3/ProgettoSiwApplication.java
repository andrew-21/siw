package it.uniroma3;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;
import it.uniroma3.model.Azienda;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.AttivitaService;
import it.uniroma3.service.AziendaService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.ResponsabileService;

@SpringBootApplication
public class ProgettoSiwApplication {
	
	@Autowired
	private AllievoService allievoService;
	
	@Autowired
	private AttivitaService attivitaService;
	
	@Autowired
	private CentroService centroRep;
	
	@Autowired
	private AziendaService aziendaRep;
	
	@Autowired
	private ResponsabileService respRep;
		
	public static void main(String[] args) {
		SpringApplication.run(ProgettoSiwApplication.class, args);
	}
	
	@PostConstruct
	public void init() {
		
		Responsabile responsabile1 = new Responsabile("Resp", "Centro1");
		Responsabile responsabile2 = new Responsabile("Resp", "Centro2");
		Responsabile direttore = new Responsabile("Direttore", "Azienda");
		
		Azienda azienda = new Azienda("Azienda", "Via Roma", direttore);
		Centro centro1 = new Centro("Centro1", "Via Uno", "prova@example.com", 064512, 2, responsabile1);
		Centro centro2 = new Centro("Centro2", "Via Due", "prova@prova.com", 064253, 3, responsabile2);
		Allievo allievo = new Allievo("Andrea", "Cona", "andrea@prova.it", "Roma", "1996-10-20", centro1);		
		Attivita attivita = new Attivita("Siw", "2018-22-06", "16:30", "Prova Uno", centro2);
		
		respRep.save(responsabile1);
		respRep.save(responsabile2);
		respRep.save(direttore);
		aziendaRep.save(azienda);
		centroRep.save(centro1);
		centroRep.save(centro2);
		allievoService.save(allievo);
		attivitaService.save(attivita);
		
		for(Allievo a : allievoService.findAll()) {
			System.out.println(a.getNome());
		}
	}
}
