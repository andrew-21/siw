package it.uniroma3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Centro;
import it.uniroma3.repository.AllievoRepository;
import it.uniroma3.repository.CentroRepository;

@Transactional
@Service
public class CentroService {
	
	@Autowired
	private CentroRepository centroRepository;
	
	@Autowired
	private AllievoRepository allievoRepository;
	
	public Centro save(Centro centro) {
		return centroRepository.save(centro);
	}
	
	public List<Centro> findAll() {
		return (List<Centro>) this.centroRepository.findAll();
	}
	
	public boolean numeroMax(Centro centro) {
		List<Allievo> allievi = (List<Allievo>) this.allievoRepository.findAll();
		if(allievi.size() >= centro.getMaxAllievi())
			return true;
		else
			return false;
	}

}
