package it.uniroma3.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Azienda {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	private String nome;
	
	@Column
	private String indirizzo;
	
	@OneToOne
	private Responsabile direttore;
	
	public Azienda() {}
	
	public Azienda(String nome, String indirizzo, Responsabile direttore) {
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.direttore = direttore;
	}
	
	@OneToMany
	@JoinColumn(name="aziendaID_centroID")
	private List<Centro> centri;
	
	@OneToMany
	@JoinColumn(name="aziendaID_allievoID")
	private List<Allievo> allievi;
	
	// Getters & Setters
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
}