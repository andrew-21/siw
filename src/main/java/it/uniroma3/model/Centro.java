package it.uniroma3.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Centro {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private String indirizzo;
	
	@Column
	private String email;
	
	@Column
	private int telefono;
	
	@Column
	private int maxAllievi;
	
	@OneToOne
	private Responsabile responsabile;
	
	@OneToMany(mappedBy="centro")
	private List<Attivita> attivita;
	
	@OneToMany(mappedBy="centro")
	private List<Allievo> allievi;
	
	public Centro() {}
	
	public Centro(String nome, String indirizzo, String email, int telefono, int maxAllievi, Responsabile responsabile) {
		this.nome = nome;
		this.indirizzo = indirizzo;
		this.email = email;
		this.telefono = telefono;
		this.maxAllievi = maxAllievi;
		this.responsabile = responsabile;
	}
	
	// Getters & Setters
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	public int getMaxAllievi() {
		return maxAllievi;
	}
	public void setMaxAllievi(int maxAllievi) {
		this.maxAllievi = maxAllievi;
	}
}