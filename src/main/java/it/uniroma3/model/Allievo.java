package it.uniroma3.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Allievo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@Column(nullable=false)
	private String cognome;
	
	@Column
	private String email;
	
	@Column
	private int telefono;
	
	@Column
	private String data;
	
	@Column
	private String luogoNascita;
	
	@ManyToMany
	private List<Attivita> attivita;
	
	@ManyToOne
	private Centro centro;
	
	public Allievo(String nome, String cognome, String email, String luogoNascita, String data, Centro centro) {
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
		this.luogoNascita = luogoNascita;
		this.data = data;
		this.centro = centro;
	}
	
	public Allievo() {}
	
	// Getters & Setters
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getLuogoNascita() {
		return luogoNascita;
	}
	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}
	public Centro getCentro() {
		return centro;
	}
	public void setCentro(Centro centro) {
		this.centro = centro;
	}
	public List<Attivita> getAttivita() {
		return this.attivita;
	}
	public void setAttivita(List<Attivita> attivita) {
		this.attivita = attivita;
	}
}