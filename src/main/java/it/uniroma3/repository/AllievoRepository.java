package it.uniroma3.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Attivita;

public interface AllievoRepository extends CrudRepository<Allievo, Long> {
		
	public Allievo findByNome(String nome);
	
	public List<Allievo> findByNomeAndCognome(String nome, String cognome);
	
	public List<Allievo> findByNomeAndCognomeAndAttivita(String nome, String cognome, Attivita attivita);

}